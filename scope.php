<?php

namespace Paco;

class FunctionPtr {
	private $ref;

	public function __construct($ref) {
		$this->ref = $ref;
	}

	public function __invoke() {
		if (is_callable($this->ref))
			return call_user_func_array($this->ref, func_get_args());
		else
			return '';
	}

	public function __toString() {
		return '';
	}
}

/**
 * Serves as a data object proxy for implementing nested scopes.
 * The proxy exposes variables as properties, which can be set
 * using the set_variable() method; variables that are not set
 * in the proxy bubble up to the fallback object, which can be
 * set through the constructor. This works just like inner and
 * outer scopes in languages like C, where the inner scope takes
 * precedence over the outer scope, and inner-scope variables
 * 'shadow' outer-scope ones.
 */
class Scope {
	/**
	 * The containing scope; can be an object or an array.
	 */
	private $fallback;

	/**
	 * Our own properties, a.k.a. local variables.
	 */
	private $properties;

	public function __construct($fallback = null) {
		$this->fallback = $fallback;
		$this->properties = array();
	}

	public function getSelf() {
		if (is_array($this->properties)) {
			return $this->fallback;
		}
		else {
			return $this->properties;
		}
	}

	public function push() {
		return new Scope($this);
	}

	public function pop() {
		if ($this->fallback instanceof Scope) {
			return $this->fallback;
		}
		else {
			throw new \Exception('Scope: bottom of scope stack reached.');
		}
	}

	/**
	 * Helper function to leniently get a value from either an object or an 
	 * array.
	 */
	public static function getFrom($obj, $key) {
		if (is_object($obj)) {
			if (isset($obj->$key))
				return $obj->$key;
			$callable = array($obj, $key);
			if (is_callable($callable))
				return new FunctionPtr($callable);
		}
		if (is_array($obj)) {
			if (!isset($obj[$key]))
				return false;
			return $obj[$key];
		}
		return false;
	}

	public static function flatten($obj) {
		if (is_array($obj)) {
			$result = array();
			foreach ($obj as $item) {
				$result[] = self::flatten($item);
			}
			return implode(' ', $result);
		}
		else {
			return (string)$obj;
		}
	}

	/**
	 * Helper function to leniently check if a property exists in either an 
	 * array or an object.
	 */
	private static function existsIn($obj, $key) {
		if (is_object($obj))
			return isset($obj->$key) || ($obj->$key === null);
		if (is_array($obj))
			return array_key_exists($key, $obj);
		return false;
	}

	public function __get($key) {
		// try the local scope first
		if (self::existsIn($this->properties, $key))
			return self::getFrom($this->properties, $key);
		// then try the containing scope
		if (self::existsIn($this->fallback, $key))
			return self::getFrom($this->fallback, $key);
		// Not found... no error here, because we want to be forgiving.
		// Serving a flawless template is more important than reporting a
		// missing value.
		return false;
	}

	/**
	 * Set a variable in the local scope. Because objects are reference types,
	 * we do not allow setting a variable if the local scope is an object,
	 * otherwise the modified variables would be passed back to the caller.
	 */
	public function __set($name, $value) {
		if (!is_array($this->properties))
			throw new Exception("Cannot set variables when using a thisobj");
		$this->properties[$name] = $value;
	}

	public function __isset($name) {
		if (self::existsIn($this->properties, $name)) {
			return true;
		}
		return self::existsIn($this->fallback, $name);
	}


	/**
	 * Remove a variable from the local scope. Because objects are reference 
	 * types, we do not allow setting a variable if the local scope is an 
	 * object, otherwise the modified variables would be passed back to the 
	 * caller.
	 */

	public function __unset($name) {
		if (is_object($this->properties))
			throw new Exception("Cannot unset variables when using a thisobj");
		unset($this->properties[$name]);
	}

	/**
	 * Completely override the local scope with a new object or array.
	 * If you provide an object, setting and unsetting local variables will be
	 * disabled.
	 */
	public function import($thisobj) {
		$this->properties = $thisobj;
	}

	/**
	 * Registers a function as a variable in the current scope.
	 * Note that only free functions need to be registered this way; all public
	 * methods are automatically available.
	 */
	public function registerFunction($name, $callable) {
		$this->__set($name, new FunctionPtr($callable));
	}
}
