<?php

require_once dirname(__FILE__) . '/ast.php';
require_once dirname(__FILE__) . '/parser.php';
require_once dirname(__FILE__) . '/phpwriter.php';
require_once dirname(__FILE__) . '/scope.php';
require_once dirname(__FILE__) . '/optimizer.php';
