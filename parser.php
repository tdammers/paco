<?php

namespace Paco;

/**
 * This exception is thrown to signal a no-parse, that is, a speculative parse
 * that failed. The parser state has not been modified, and the parent parser
 * is expected to continue using a different speculative parse.
 */
class NoParse extends \Exception { }

/**
 * This exception is thrown to signal an actual parser error. The parser state
 * may or may not have been modified, and the parent parser is expected to
 * bubble the exception or abort and print the exception message.
 */
class FatalError extends \Exception { }

class ParserState {
	private $source;
	private $sourceFile;
	private $sourceLine;
	private $sourceColumn;
	private $sourcePosition;
	private $parent;
	private $recursionDepth;
	private $localDefs;

	public function __construct($source, $sourceFile = '<input>', $sourceLine = 1, $sourceColumn = 1, $sourcePosition = 0, $parent = null) {
		$this->source = $source;
		$this->sourcePosition = $sourcePosition;
		$this->sourceLine = $sourceLine;
		$this->sourceColumn = $sourceColumn;
		$this->sourceFile = $sourceFile;
		$this->parent = $parent;
		if ($parent) {
			$this->recursionDepth = $parent->recursionDepth + 1;
		}
		else {
			$this->recursionDepth = 0;
		}
		$this->localDefs = array();
	}

	public function __get($key) {
		switch ($key) {
			case 'sourceCurrent':
				return substr($source, $this->sourcePosition);
			case 'localDefs':
				return $this->localDefs;
			default:
				return parent::__get($key);
		}
	}

	/**
	 * Create a new entry on the parser stack and return it. This is used for
	 * speculative parsing: the speculative parse is executed with the new
	 * state; when it succeeds, the pushed state is copied back into the parent
	 * state using the confirm() method, if it fails, the parent parser simply
	 * picks up the unmodified parent state.
	 */
	public function push() {
		return new ParserState(
				$this->source,
				$this->sourceFile,
				$this->sourceLine,
				$this->sourceColumn,
				$this->sourcePosition,
				$this);
	}

	/**
	 * Confirm a speculative parse by copying the child parser state into the
	 * current state.
	 */
	public function confirm(ParserState $rhs) {
		$this->source = $rhs->source;
		$this->sourcePosition = $rhs->sourcePosition;
		$this->sourceLine = $rhs->sourceLine;
		$this->sourceColumn = $rhs->sourceColumn;
		$this->sourceFile = $rhs->sourceFile;
	}

	public function dumpSourcePosition() {
		if (!$this->isEof()) {
			$left = $this->sourcePosition - $this->sourceColumn + 1;
			$width = strpos($this->source, "\n", $this->sourcePosition);
			if ($width === false)
				$width = strlen($this->source) - $left;
			$markerPos = $this->sourcePosition - $left;
			$sourceFragment = substr($this->source, $left, $width);
			fprintf(STDERR, "%s\n", $sourceFragment);
			if ($width > 0)
				fprintf(STDERR, "%s^\n", str_repeat(" ", $markerPos));
		}
	}

	/**
	 * For internal use: report a failure, either fatal (hard), or non-fatal
	 * (i.e., a no-parse exception).
	 */
	private function _fail($message, $fatal = false) {
		$message = sprintf("Parser error: %s, line %d, col %d: %s",
				$this->sourceFile,
				$this->sourceLine,
				$this->sourceColumn,
				$message);
		if ($fatal)
			throw new FatalError($message);
		else
			throw new NoParse($message);
	}

	/**
	 * Report a fatal (non-recoverable) error.
	 */
	public function failFatal($message) {
		$this->_fail($message, true);
	}

	/**
	 * Report a no-parse, that is, a failed speculative parse.
	 */
	public function fail($message) {
		$this->_fail($message, false);
	}

	/**
	 * Consume one or more characters from the input.
	 */
	public function consume($count = 1) {
		for ($i = 0; $i < $count; ++$i) {
			$this->nextChar();
		}
	}

	/**
	 * Check whether end-of-input has been reached.
	 */
	public function isEof() {
		return ($this->sourcePosition >= strlen($this->source));
	}

	/**
	 * Unconditionally get and consume one character.
	 */
	public function nextChar() {
		if ($this->isEof()) {
			$this->failFatal("Unexpected end of input");
		}
		$c = $this->source[$this->sourcePosition++];
		if ($c == "\n") {
			$this->sourceLine++;
			$this->sourceColumn = 1;
		}
		else {
			$this->sourceColumn++;
		}
		return $c;
	}

	/**
	 * Verify that the next character matches the expected character $e.
	 * If $hard is FALSE, a no-parse is thrown, otherwise, a fatal error is
	 * signalled.
	 * Note that this function always consumes the checked character.
	 */
	public function verifyNext($e, $hard = true) {
		$c = $this->nextChar();
		if ($c !== $e)
			$this->_fail("Unexpected '$c', expected '$e'", $hard);
	}

	/**
	 * Verify that the next characters match the expected string $str.
	 * If $hard is FALSE, a no-parse is thrown, otherwise, a fatal error is
	 * signalled.
	 * Note that this function always consumes the checked characters, either
	 * completely, or up to and including the point of failure.
	 */
	public function verifyString($str, $hard = true) {
		foreach (str_split($str) as $e) {
			$this->verifyNext($e, $hard);
		}
	}

	/**
	 * Reads the exact string $str from the stream and consumes it. Fails with
	 * a no-parse if the stream does not match the string.
	 */
	public function str($str) {
		$length = strlen($str);
		$cur = substr($this->source, $this->sourcePosition, $length);
		if ($cur !== $str) {
			$this->fail("Unexpected '$cur', expected '$str'");
		}
		$this->consume($length);
		return $str;
	}

	/**
	 * Single-character lookahead. Gets one character without consuming it.
	 */
	public function peekChar() {
		if ($this->isEof()) {
			$this->failFatal("Unexpected end of input");
		}
		return $this->source[$this->sourcePosition];
	}

	/**
	 * Checks the input from the current position against the specified regex.
	 * If it matches, the match array is returned (as returned by preg_match),
	 * otherwise, a no-parse error is thrown.
	 */
	public function peekRegex($regex) {
		$isMatch = preg_match($regex, substr($this->source, $this->sourcePosition), $match);
		if ($isMatch) {
			return $match;
		}
		else {
			$this->fail("Unexpected input, expected match on $regex");
		}
	}

	public function regex($regex) {
		$match = $this->peekRegex($regex);
		$count = strlen($match[0]);
		$this->consume($count);
		return $match;
	}

	public function oneOf($strings) {
		foreach ($strings as $s) {
			try { return $this->str($s); }
			catch (NoParse $ex) { }
		}
		$this->fail("Expected one of (" . implode(", ", $strings) . ")");
	}

	public function localDef($name, ASTStatement $body) {
		$this->localDefs[$name] = $body;
	}

	public function importDefs($defs) {
		foreach ($defs as $name => $body)
			$this->localDef($name, $body);
	}

	public function resolveDef($name) {
		if (isset($this->localDefs[$name]))
			return $this->localDefs[$name];
		elseif ($this->parent)
			return $this->parent->resolveDef($name);
		else
			return null;
	}
}

class Parser {
	protected static function skipWhitespace(ParserState $state) {
		$state->regex('/^[\\s\\r\\n]*/');
	}

	///// Expression parsers ///// 

	protected static function parseIdentifier(ParserState $state) {
		$match = $state->regex('/^([a-zA-Z_\-][a-zA-Z_\-0-9]*)|^\\./');
		return $match[0];
	}

	protected static function parseStringLiteral(ParserState $state) {
		$substate = $state->push();
		$delim = $substate->nextChar();
		switch ($delim) {
			case '"':
			case "'":
				break;
			default:
				$state->fail("Unexpected '$delim', expected string literal");
		}
		$str = '';
		$done = false;
		do {
			$c = $substate->nextChar();
			switch ($c) {
				case '\\':
					$c = $substate->nextChar();
					$str .= $c;
					break;
				case $delim:
					$done = true;
					break;
				default:
					$str .= $c;
			}
		} while (!$done);
		$state->confirm($substate);
		return new LiteralExpression($str);
	}

	protected static function parseListExpression(ParserState $state) {
		if ($state->peekChar() !== '[') {
			$state->fail("Expected '['");
		}
		$state->verifyNext('[');
		$valid = true;
		$list = new ListLiteralExpression();
		while ($valid) {
			self::skipWhitespace($state);
			if ($state->peekChar() === ']') {
				$valid = false;
			}
			else {
				$list->append(self::parseExpression($state));
				self::skipWhitespace($state);
				if ($state->peekChar() === ',') {
					$state->consume(1);
					self::skipWhitespace($state);
				}
			}
		}
		$state->verifyNext(']');
		return $list;
	}

	protected static function parseMapExpression(ParserState $state) {
		if ($state->peekChar() !== '{') {
			$state->fail("Expected '{'");
		}
		$state->verifyNext('{');
		$valid = true;
		$list = new MapLiteralExpression();
		while ($valid) {
			self::skipWhitespace($state);
			if ($state->peekChar() === '}') {
				$valid = false;
			}
			else {
				$keyExpr = self::parseExpression($state);
				self::skipWhitespace($state);
				$state->verifyNext(':');
				self::skipWhitespace($state);
				$valueExpr = self::parseExpression($state);
				$list->append($keyExpr, $valueExpr);
				self::skipWhitespace($state);
				if ($state->peekChar() === ',') {
					$state->consume(1);
					self::skipWhitespace($state);
				}
			}
		}
		$state->verifyNext('}');
		return $list;
	}

	protected static function parseDecimalLiteral(ParserState $state) {
		$match = $state->regex('/^\\-?[1-9][0-9]*\.[0-9]*/');
		return new LiteralExpression($match[0]);
	}

	protected static function parseIntegerLiteral(ParserState $state) {
		$match = $state->regex('/^\\-?[1-9][0-9]*|^0/');
		return new LiteralExpression($match[0]);
	}

	protected static function parseVariableRef(ParserState $state) {
		$name = self::parseIdentifier($state);
		return new VariableExpression($name);
	}

	protected static function parseGroupedExpression(ParserState $state) {
		if ($state->peekChar() !== '(') {
			$state->fail("Unexpected {$state->peekChar()}, expected '('");
		}
		$state->consume(1);
		self::skipWhitespace($state);
		$inner = self::parseExpression($state);
		self::skipWhitespace($state);
		$state->verifyNext(')');
		return $inner;
	}

	protected static function parseUnaryExpression(ParserState $state, $rightParser, $operators) {
		$substate = $state->push();
		self::skipWhitespace($substate);
		$quoteFunc = function ($q) { return str_replace('/', '\\/', preg_quote($q)); };
		$regex = '/^(' . implode('|', array_map($quoteFunc, $operators)) . ')(?:$|[^+\-*\/=!<>&|])/';
		$match = $substate->peekRegex($regex);
		$operator = $match[1];
		$substate->consume(strlen($operator));
		self::skipWhitespace($substate);
		$rhs = call_user_func('self::' . $rightParser, $substate);
		$state->confirm($substate);
		return new UnaryExpression($operator, $rhs);
	}

	protected static function parseBinaryExpressionR(ParserState $state, $leftParser, $rightParser, $operators) {
		$substate = $state->push();
		$lhs = call_user_func('self::' . $leftParser, $substate);
		self::skipWhitespace($substate);
		try {
			$quoteFunc = function ($q) { return str_replace('/', '\\/', preg_quote($q)); };
			$regex = '/^(' . implode('|', array_map($quoteFunc, $operators)) . ')(?:$|[^+\-*\/=!<>&|])/';
			$match = $substate->peekRegex($regex);
			$operator = $match[1];
			$substate->consume(strlen($operator));
		}
		catch (NoParse $ex) {
			$state->confirm($substate);
			return $lhs;
		}
		self::skipWhitespace($substate);
		$rhs = call_user_func('self::' . $rightParser, $substate);
		$state->confirm($substate);
		return new BinaryExpression($operator, $lhs, $rhs);
	}

	protected static function parseBinaryExpressionL(ParserState $state, $leftParser, $rightParser, $operators) {
		$lhs = call_user_func('self::' . $leftParser, $state);
		$valid = true;
		while ($valid) {
			try {
				$substate = $state->push();
				$quoteFunc = function ($q) { return str_replace('/', '\\/', preg_quote($q)); };
				$regex = '/^(' . implode('|', array_map($quoteFunc, $operators)) . ')(?:$|[^+\-*\/=!<>&|])/';
				$match = $substate->peekRegex($regex);
				$operator = $match[1];
				$substate->consume(strlen($operator));

				self::skipWhitespace($substate);
				$rhs = call_user_func('self::' . $leftParser, $substate);
				self::skipWhitespace($substate);
				$state->confirm($substate);
				$lhs = new BinaryExpression($operator, $lhs, $rhs);

			}
			catch (NoParse $ex) {
				$valid = false;
			}
		}
		self::skipWhitespace($state);
		return $lhs;
	}

	protected static function parseLogicExpression(ParserState $state) {
		try { return self::parseBinaryExpressionL($state, 'parseListElementTestExpression', 'parseLogicExpression', array('&&', '||', 'and', 'or')); }
		catch (NoParse $ex) { /* no parse, try next. */  }
		return self::parseListElementTestExpression($state);
	}

	protected static function parseListElementTestExpression(ParserState $state) {
		try { return self::parseBinaryExpressionL($state, 'parseComparativeExpression', 'parseListElementTestExpression', array('in', 'contains')); }
		catch (NoParse $ex) { /* no parse, try next. */  }
		return self::parseComparativeExpression($state);
	}

	protected static function parseComparativeExpression(ParserState $state) {
		try { return self::parseBinaryExpressionL($state, 'parseConcatenativeExpression', 'parseComparativeExpression', array('>=', '<=', '<>', '>', '<', '===', '==', '=~', '=', '~=', '!==', '!=')); }
		catch (NoParse $ex) { /* no parse, try next. */  }
		return self::parseConcatenativeExpression($state);
	}

	protected static function parseConcatenativeExpression(ParserState $state) {
		try { return self::parseBinaryExpressionL($state, 'parseAdditiveExpression', 'parseConcatenativeExpression', array('++')); }
		catch (NoParse $ex) { /* no parse, try next. */  }
		return self::parseAdditiveExpression($state);
	}

	protected static function parseAdditiveExpression(ParserState $state) {
		try { return self::parseBinaryExpressionL($state, 'parseMultiplicativeExpression', 'parseAdditiveExpression', array('+', '-')); }
		catch (NoParse $ex) { /* no parse, try next. */  }
		return self::parseMultiplicativeExpression($state);
	}

	protected static function parseMultiplicativeExpression(ParserState $state) {
		try { return self::parseBinaryExpressionL($state, 'parseBitwiseExpression', 'parseMultiplicativeExpression', array('*', '/')); }
		catch (NoParse $ex) { /* no parse, try next. */  }
		return self::parseBitwiseExpression($state);
	}

	protected static function parseBitwiseExpression(ParserState $state) {
		try { return self::parseBinaryExpressionL($state, 'parseUnaryOperatorExpression', 'parseBitwiseExpression', array('&', '|')); }
		catch (NoParse $ex) { /* no parse, try next. */  }
		return self::parseUnaryOperatorExpression($state);
	}

	protected static function parseUnaryOperatorExpression(ParserState $state) {
		try { return self::parseUnaryExpression($state, 'parseJuxtaposedFunctionCallExpression', array('not', '-', '~', '!')); }
		catch (NoParse $ex) { /* no parse, try next. */  }
		return self::parseJuxtaposedFunctionCallExpression($state);
	}

	protected static function parseJuxtaposedFunctionCallExpression(ParserState $state) {
		if ($state->peekChar() !== '$') {
			return self::parseFunctionCallExpression($state);
		}
		$state->verifyNext('$');
		$func = self::parseExpression($state);
		$call = new FunctionCallExpression($func);
		$valid = true;
		while ($valid) {
			$substate = $state->push();
			try {
				self::skipWhitespace($substate);
				$arg = self::parseExpression($substate, false);
				$call->addArgument($arg);
				$state->confirm($substate);
			}
			catch (NoParse $ex) {
				$valid = false;
			}
		}
		return $call;
	}

	protected static function parseFunctionCallExpression(ParserState $state) {
		$lhs = self::parseDereferenceExpression($state);
		self::skipWhitespace($state);
		if ($state->isEof() || $state->peekChar() !== '(') {
			return $lhs;
		}
		$state->verifyNext('(');
		$more = true;
		$lhs = new FunctionCallExpression($lhs);
		do {
			self::skipWhitespace($state);
			switch ($state->peekChar()) {
				case ')':
					$state->verifyNext(')');
					$more = false;
					break;
				case ',':
					$state->consume(1);
					continue;
				default:
					$arg = self::parseExpression($state);
					$lhs->addArgument($arg);
					break;
			}
		} while ($more);
		return $lhs;
	}

	protected static function parseDereferenceExpression(ParserState $state) {
		$lhs = self::parseSimpleExpression($state);

		self::skipWhitespace($state);
		$valid = true;
		do {
			switch ($state->peekChar()) {
				case '[':
					$state->verifyNext('[');
					self::skipWhitespace($state);
					$rhs = self::parseExpression($state);
					self::skipWhitespace($state);
					$state->verifyNext(']');

					$state->confirm($state);
					$lhs = new BinaryExpression('.', $lhs, $rhs);
					break;
				case '.':
					$state->verifyNext('.');
					$member = self::parseIdentifier($state);
					self::skipWhitespace($state);

					$state->confirm($state);
					$rhs = new LiteralExpression($member);
					$lhs = new BinaryExpression('.', $lhs, $rhs);
					break;
				default:
					$valid = false;
					break;
			}
		} while ($valid);
		return $lhs;
	}

	protected static function parseSimpleExpression(ParserState $state) {
		try { return self::parseStringLiteral($state); }
		catch (NoParse $ex) { /* no parse, try next. */  }

		try { return self::parseDecimalLiteral($state); }
		catch (NoParse $ex) { /* no parse, try next. */  }

		try { return self::parseIntegerLiteral($state); }
		catch (NoParse $ex) { /* no parse, try next. */  }

		try { return self::parseVariableRef($state); }
		catch (NoParse $ex) { /* no parse, try next. */  }

		try { return self::parseListExpression($state); }
		catch (NoParse $ex) { /* no parse, try next. */  }

		try { return self::parseMapExpression($state); }
		catch (NoParse $ex) { /* no parse, try next. */  }

		try { return self::parseGroupedExpression($state); }
		catch (NoParse $ex) { /* no parse, try next. */  }

		$state->fail("Expected simple expression");
	}


	protected static function parseExpression(ParserState $state, $fatal = true) {
		try { return self::parseLogicExpression($state); }
		catch (NoParse $ex) { /* no parse, try next. */  }

		try { return self::parseSimpleExpression($state); }
		catch (NoParse $ex) { /* no parse, try next. */  }

		if ($fatal)
			$state->failFatal("Expected expression");
		else
			$state->fail("Expected expression");
	}

	///// Statement parsers ///// 

	protected static function skipNewline(ParserState $state) {
		if ($state->isEof()) {
			return false;
		}
		if ($state->peekChar() === "\n") {
			$state->consume(1);
			return true;
		}
		else {
			return false;
		}
	}

	protected static function parseText(ParserState $state) {
		$match = $state->regex('/^[^{]+/');
			return new TextStatement($match[0]);
		}

		protected static function parseValueOf(ParserState $state) {
			$substate = $state->push();
			$substate->verifyNext('{', false);
			$encodeMode = null;
			$c = $substate->peekChar();
			switch ($c) {
				case '@': $encodeMode = ENCODE_URL; break;
				case '!': $encodeMode = ENCODE_RAW; break;
				case '%': $substate->fail("Not a value-of construct");
			}
			if ($encodeMode !== null) {
				$substate->nextChar();
			}
			else {
				$encodeMode = ENCODE_HTML;
			}
			self::skipWhitespace($substate);
			$expr = self::parseExpression($substate);
			self::skipWhitespace($substate);
			$substate->verifyNext('}');
			$state->confirm($substate);
			return new ValueOfStatement($expr, $encodeMode);
		}

		protected static function parseCaseBranch(ParserState $state, $includeHandler) {
			$substate = $state->push();
			$match = $substate->regex('/^\\{\\%\\s*case\\s/');
				self::skipWhitespace($substate);
				$condition = self::parseExpression($substate);
				self::skipWhitespace($substate);
				$substate->verifyString('%}');
			self::skipNewline($substate);
			$body = self::parseBlock($substate, $includeHandler);
			try {
				$substate->regex('/^\\{\\%\\s*endcase\\s*\\%\\}/');
				self::skipNewline($substate);
			}
			catch (NoParse $ex) {
				$substate->failFatal("Expected {%endcase%}");
			}
			self::skipNewline($substate);
			$state->confirm($substate);
			return new CaseBranchStatement($condition, $body);
		}

		protected static function parseDefaultBranch(ParserState $state, $includeHandler) {
			$substate = $state->push();
			$match = $substate->regex('/^\\{\\%\\s*(default|otherwise)\\s*%}/');
			self::skipNewline($substate);
			$body = self::parseBlock($substate, $includeHandler);
			try {
				$substate->regex('/^\\{\\%\\s*endcase\\s*\\%\\}/');
				self::skipNewline($substate);
			}
			catch (NoParse $ex) {
				$substate->failFatal("Expected {%endcase%}");
			}
			self::skipNewline($substate);
			$state->confirm($substate);
			return $body;
		}

		protected static function parseSwitch(ParserState $state, $includeHandler) {
			$substate = $state->push();
			$match = $substate->regex('/^\\{\\%\\s*switch\\s/');
				self::skipWhitespace($substate);
				$condition = self::parseExpression($substate);
				self::skipWhitespace($substate);
				$substate->verifyString('%}');
			self::skipNewline($substate);
			$switch = new SwitchStatement($condition);
			$valid = true;
			while ($valid) {
				self::skipWhitespace($substate);
				try {
					$branch = self::parseCaseBranch($substate, $includeHandler);
					$switch->addBranch($branch);
					continue;
				}
				catch (NoParse $ex) {
				}
				try {
					$branch = self::parseDefaultBranch($substate, $includeHandler);
					$switch->setDefault($branch);
					continue;
				}
				catch (NoParse $ex) {
				}
				$valid = false;
			}
			try {
				$substate->regex('/^\\{\\%\\s*endswitch\\s*\\%\\}/');
				self::skipNewline($substate);
			}
			catch (NoParse $ex) {
				$substate->failFatal("Expected {%endswitch%}");
			}
			$state->confirm($substate);
			return $switch;
		}

		protected static function parseIf(ParserState $state, $includeHandler) {
			$substate = $state->push();
			$match = $substate->regex('/^\\{\\%\\s*if\\s/');
			self::skipWhitespace($substate);
			$condition = self::parseExpression($substate);
			self::skipWhitespace($substate);
			$substate->verifyString('%}');
			self::skipNewline($substate);
			$trueBranch = self::parseBlock($substate, $includeHandler);
			try {
				$match = $substate->regex('/^\\{\\%\\s*(else|endif)\\s*\\%\\}/');
				self::skipNewline($substate);
			}
			catch (NoParse $ex) {
				$substate->failFatal("Expected {%else%} or {%endif%}");
			}
			self::skipNewline($substate);
			if ($match[1] === 'else') {
				$falseBranch = self::parseBlock($substate, $includeHandler);
				try {
					$substate->regex('/^\\{\\%\\s*endif\\s*\\%\\}/');
					self::skipNewline($substate);
				}
				catch (NoParse $ex) {
					$substate->failFatal("Expected {%endif%}");
				}
			}
			else {
				$falseBranch = null;
			}
			self::skipNewline($substate);
			$state->confirm($substate);
			return new IfStatement($condition, $trueBranch, $falseBranch);
		}

		protected static function parseColonAssignment(ParserState $state) {
			self::skipWhitespace($state);
			$selectExpr = self::parseExpression($state);
			self::skipWhitespace($state);
			if ($state->peekChar() === ':') {
				$state->nextChar();
				try {
					self::skipWhitespace($state);
					$localName = self::parseIdentifier($state);
					self::skipWhitespace($state);
				}
				catch (NoParse $ex) {
					$state->failFatal("Expected identifier name");
				}
			}
			else {
				$localName = '';
			}
			return array($selectExpr, $localName);
		}

		protected static function parseWithOrFor(ParserState $state, $includeHandler) {
			$substate = $state->push();
			$match = $substate->regex('/^\\{\\%\\s*(with|for)\\s/');
				$kind = $match[1];

				if ($kind == 'with') {
					$colons = array();
					do {
						$colons[] = self::parseColonAssignment($substate);
						if ($substate->peekChar() === ',') {
							$substate->consume(1);
							self::skipWhitespace($substate);
							$valid = true;
						}
						else {
							$valid = false;
						}
					} while ($valid);
				}
				else {
					list($selectExpr, $localName) = self::parseColonAssignment($substate);
				}

				$substate->verifyString('%}');
			self::skipNewline($substate);
			$inner = self::parseBlock($substate, $includeHandler);
			try {
				$substate->regex("/^\\{\\%\\s*end$kind\\s*\\%\\}/");
				self::skipNewline($substate);
			}
			catch (NoParse $ex) {
				$substate->failFatal("Expected {%end$kind%}");
			}
			self::skipNewline($substate);
			$state->confirm($substate);
			switch ($kind) {
				case 'with':
					$node = $inner;
					// we need to process colons back-to-front so that later colon
					// pairs inherit their predecessors' scope
					foreach (array_reverse($colons) as $colon) {
						list($selectExpr, $localName) = $colon;
						$node = new WithStatement($selectExpr, $localName, $node);
					}
					return $node;

				case 'for': return new ForStatement($selectExpr, $localName, $inner);
				default: $substate->failFatal("Unexpected '$kind', expected 'for' or 'with'");
			}
		}

		protected static function skipComment(ParserState $state) {
			$state->regex('/^\\{\\%\\-\\-/');
				while (1) {
					if ($state->isEof()) {
						return null;
					}
					switch ($state->peekChar()) {
						case '-':
							try {
								$state->str('--%}');
							self::skipNewline($state);
							return null;
					}
					catch (NoParse $ex) {
						$state->nextChar();
					}
					break;
					case '{':
					try {
						self::skipComment($state);
					}
					catch (NoParse $ex) {
						$state->nextChar();
					}
					break;
					default:
					$state->nextChar();
					break;
				}
			}
		}

		protected static function parseTemplateName($state) {
			$path = '';
			$running = true;
			do {
				try {
					if ($state->peekChar() === '/')
						$part = $state->nextChar();
					else
						$part = '';
					$match = $state->regex('/^[a-zA-Z0-9_-]/');
					$path .= $part . $match[0];
				}
				catch (NoParse $ex) {
					$running = false;
				}
			} while ($running);
			return $path;
		}

		protected static function parseInclude(ParserState $state, $includeHandler) {
			$scopeExpr = null;
			$localName = null;
			$substate = $state->push();
			$substate->regex('/^\\{\\%\\s*include\\s/');
				self::skipWhitespace($substate);
				$templateName = self::parseTemplateName($substate);
				self::skipWhitespace($substate);
				if ($substate->peekChar() !== '%') {
					$substate->str('with');
					self::skipWhitespace($substate);
					$scopeExpr = self::parseExpression($substate);
					self::skipWhitespace($substate);
					if ($substate->peekChar() === ':') {
						$substate->consume(1);
						self::skipWhitespace($substate);
						$localName = self::parseIdentifier($substate);
						self::skipWhitespace($substate);
					}
				}
				$substate->str('%}');
			self::skipNewline($substate);
			$state->confirm($substate);
			$filename = '<input>';
			$includedSrc = $includeHandler->loadInclude($templateName, $filename);
			$includedState = new ParserState($includedSrc, $filename);
			$includedState->importDefs($state->localDefs);
			$includedBody = self::parseWithState($includedState, $includeHandler);
			$state->importDefs($includedState->localDefs);
			if ($scopeExpr) {
				return new WithStatement($scopeExpr, $localName, $includedBody);
			}
			else {
				return $includedBody;
			}
		}

		protected static function parseCall(ParserState $state, $callHandler) {
			$scopeExpr = null;
			$localName = null;
			$substate = $state->push();
			$substate->regex('/^\\{\\%\\s*call\\s/');
				self::skipWhitespace($substate);
				try {
					$defName = self::parseIdentifier($substate);
				}
				catch (NoParse $ex) {
					$substate->failFatal("Invalid def identifier");
				}
				self::skipWhitespace($substate);
				if ($substate->peekChar() !== '%') {
					$substate->str('with');
					self::skipWhitespace($substate);
					$scopeExpr = self::parseExpression($substate);
					self::skipWhitespace($substate);
					if ($substate->peekChar() === ':') {
						$substate->consume(1);
						self::skipWhitespace($substate);
						$localName = self::parseIdentifier($substate);
						self::skipWhitespace($substate);
					}
				}
				$substate->str('%}');
			self::skipNewline($substate);
			$state->confirm($substate);
			$body = $state->resolveDef($defName);
			if ($scopeExpr) {
				return new WithStatement($scopeExpr, $localName, $body);
			}
			else {
				return $body;
			}
		}

		protected static function parseDef(ParserState $state, $includeHandler) {
			$substate = $state->push();
			$substate->regex('/^\\{\\%\\s*def\\s/');
				self::skipWhitespace($substate);
				try {
					$defName = self::parseIdentifier($substate);
				}
				catch (NoParse $ex) {
					$substate->failFatal("Invalid def identifier");
				}
				self::skipWhitespace($substate);
				$substate->str('%}');
			self::skipNewline($substate);
			self::skipNewline($substate);
			$body = self::parseBlock($substate, $includeHandler);
			try {
				$substate->regex("/^\\{\\%\\s*enddef\\s*\\%\\}/");
				self::skipNewline($substate);
			}
			catch (NoParse $ex) {
				$substate->failFatal("Expected {%enddef%}");
			}
			$state->confirm($substate);
			$state->localDef($defName, $body);
			return null;
		}

		protected static function parseBlockElement(ParserState $state, $includeHandler) {
			try { return self::skipComment($state); }
			catch (NoParse $ex) { /* done skipping comments */ }

			try { return self::parseDef($state, $includeHandler); }
			catch (NoParse $ex) { /* no parse, try next */ }

			try { return self::parseCall($state, $includeHandler); }
			catch (NoParse $ex) { /* no parse, try next */ }

			try { return self::parseInclude($state, $includeHandler); }
			catch (NoParse $ex) { /* no parse, try next */ }

			try { return self::parseSwitch($state, $includeHandler); }
			catch (NoParse $ex) { /* no parse, try next */ }

			try { return self::parseIf($state, $includeHandler); }
			catch (NoParse $ex) { /* no parse, try next */ }

			try { return self::parseWithOrFor($state, $includeHandler); }
			catch (NoParse $ex) { /* no parse, try next */ }

			try { return self::parseValueOf($state); }
			catch (NoParse $ex) { /* no parse, try next */ }

			try { return self::parseText($state); }
			catch (NoParse $ex) { /* no parse, try next */ }

			$state->fail("Expected value interpolation or text");
		}

		protected static function parseBlock(ParserState $state, $includeHandler) {
			$node = new BlockStatement();
			$matching = true;
			$hasElems = false;
			while ($matching) {
				if ($state->isEof()) {
					$matching = false;
					continue;
				}
				try {
					$e = self::parseBlockElement($state, $includeHandler);
					if ($e) {
						$node->append($e);
						$hasElems = true;
					}
				}
				catch (NoParse $ex) {
					$matching = false;
				}
			}
			return $node;
		}

		public static function parse($string, $filename = '<input>', $includeHandler = null, $returnState = false) {
			$state = new ParserState($string, $filename);
			$block = self::parseWithState($state, $includeHandler);
			if ($returnState)
				return array($state, $block);
			else
				return $block;
		}

		protected static function parseWithState($state, $includeHandler) {
			$block = self::parseBlock($state, $includeHandler);
			if (!$state->isEof()) {
				$state->failFatal("Expected end-of-input");
			}
			return $block;
		}
	}
