<?php

namespace Paco;

class PhpWriter implements Writer {
	private $indentCount;
	private $varcounter;
	private $header;
	private $footer;
	private $classname;

	public function __construct($classname = null, $header = '', $footer = '') {
		$this->indentCount = 0;
		$this->varcounter = 1;
		$this->classname = $classname;
		$this->header = $header;
		$this->footer = $footer;
	}

	private function indent() {
		return str_repeat("\t", $this->indentCount);
	}

	private function pushIndent() { $this->indentCount++; }
	private function popIndent() { $this->indentCount--; }

	private function nextLocalVarname() {
		return "_var" . $this->varcounter++;
	}

	private function quote($value) {
		$search = array("\\", "'", "\n");
		$replace = array("\\\\", "\\'", "' . \"\\n\" . '");
		return "'" . str_replace($search, $replace, $value) . "'";
	}

	private function getBinaryExpressionPattern($operator) {
		switch ($operator) {
				case '!=':
				case '!==':
				case '&&':
				case '&':
				case '*':
				case '+':
				case '-':
				case '/':
				case '<':
				case '<=':
				case '<>':
				case '>':
				case '>=':
				case 'and':
				case 'or':
				case '|':
				case '||':
					return "(%s $operator %s)";
				case '=':
					return "(%s == %s)";
				case '==':
					return "(%s === %s)";
				case '=~':
					return "preg_match(%2\$s, %1\$s)";
				case '~=':
					return "preg_match(%s, %s)";
				case '.':
					return "Paco\\Scope::getFrom(%s, %s)";
				case '++':
					return "(%s . %s)";
				case 'in':
					return "in_array(%s, %s)";
				case 'contains':
					return 'in_array(%2$s, %1$s)';
		}
	}

	private function writeBinaryExpression(BinaryExpression $n) {
		$l = $this->writeExpression($n->lhs);
		$r = $this->writeExpression($n->rhs);
		$pattern = $this->getBinaryExpressionPattern($n->operator);
		return sprintf($pattern, $l, $r);
	}

	private function getUnaryExpressionPattern($operator) {
		switch ($operator) {
			case 'not':
				return "!(%s)";
			default:
				return "$operator(%s)";
		}
	}

	private function writeUnaryExpression(UnaryExpression $n) {
		$r = $this->writeExpression($n->rhs);
		$pattern = $this->getUnaryExpressionPattern($n->operator);
		return sprintf($pattern, $r);
	}


	private function writeListExpression(ListLiteralExpression $n) {
		$str = 'array(';
		$values = array();
		foreach ($n->items as $item) {
			$values[] = self::writeExpression($item);
		}
		$str .= implode(', ', $values);
		$str .= ')';
		return $str;
	}

	private function writeMapExpression(MapLiteralExpression $n) {
		$str = 'array(';
		$values = array();
		foreach ($n->items as $item) {
			$key = self::writeExpression($item[0]);
			$value = self::writeExpression($item[1]);
			$values[] = "($key) => ($value)";
		}
		$str .= implode(', ', $values);
		$str .= ')';
		return $str;
	}

	private function writeFunctionCallExpression(FunctionCallExpression $n) {
		$funcname = $this->writeExpression($n->function);
		$args = array();
		foreach ($n->arguments as $argExp) {
			$args[] = $this->writeExpression($argExp);
		}
		$args = implode(', ', $args);
		return sprintf("((%1\$s instanceof Paco\FunctionPtr) ? (%1\$s->__invoke(%2\$s)) : '')", $funcname, $args);
	}

	private function writeValue($value) {
		if (is_array($value)) {
			$items = array();
			foreach ($value as $k => $v) {
				$items[] = sprintf('%s => %s', $this->writeValue($k), $this->writeValue($v));
			}
			return 'array(' . implode(', ', $items) . ')';
		}
		if (is_string($value)) {
			return $this->quote($value);
		}
		if (is_numeric($value)) {
			return $value;
		}
		else {
			return $this->quote($value);
		}
	}

	private function writeLiteral(LiteralExpression $n) {
		return $this->writeValue($n->value);
	}

	private function writeVariable(VariableExpression $n) {
		if ($n->varname === '.') {
			return "\$scope->getSelf()";
		}
		else {
			return sprintf("\$scope->__get(%s)",
					$this->quote($n->varname));
		}
	}

	private function writeExpression(ASTExpression $n) {
		if ($n instanceof LiteralExpression) {
			return $this->writeLiteral($n);
		}
		elseif ($n instanceof VariableExpression) {
			return $this->writeVariable($n);
		}
		elseif ($n instanceof UnaryExpression) {
			return $this->writeUnaryExpression($n);
		}
		elseif ($n instanceof BinaryExpression) {
			return $this->writeBinaryExpression($n);
		}
		elseif ($n instanceof FunctionCallExpression) {
			return $this->writeFunctionCallExpression($n);
		}
		elseif ($n instanceof ListLiteralExpression) {
			return $this->writeListExpression($n);
		}
		elseif ($n instanceof MapLiteralExpression) {
			return $this->writeMapExpression($n);
		}
		else {
			return '';
		}
	}

	private function writeValueOf(ValueOfStatement $n) {
		switch ($n->encodeMode) {
			case ENCODE_URL:
				$pre = 'rawurlencode(';
				$post = ')';
				break;

			case ENCODE_RAW:
				$pre = '';
				$post = '';
				break;

			case ENCODE_HTML:
			default:
				$pre = 'htmlspecialchars(';
				$post = ')';
				break;
		}
		$expr = $this->writeExpression($n->expr);
		$indent = $this->indent();

		return "{$indent}echo {$pre}Paco\Scope::flatten($expr)$post;\n";
	}

	private function writeText(TextStatement $n) {
		return sprintf("%secho %s;\n",
			$this->indent(),
			$this->quote($n->text));
	}

	private function writeIf(IfStatement $n) {
		$str = sprintf("%sif (%s) {\n",
			$this->indent(),
			$this->writeExpression($n->condition));
		$this->pushIndent();
		$str .= $this->writeStatement($n->trueBranch);
		$this->popIndent();
		$str .= sprintf("%s}\n",
				$this->indent());
		if ($n->falseBranch) {
			$str .= sprintf("%selse {\n",
					$this->indent());
			$this->pushIndent();
			$str .= $this->writeStatement($n->falseBranch);
			$this->popIndent();
			$str .= sprintf("%s}\n",
					$this->indent());
		}
		return $str;
	}

	private function writeSwitch(SwitchStatement $n) {
		$varname = $this->nextLocalVarname();
		$cmp = '==';
		$cases = array();
		foreach ($n->cases as $branch) {
			$str = sprintf("if (\$$varname $cmp %s) {\n", $this->writeExpression($branch->condition));
			$this->pushIndent();
			$str .= $this->writeStatement($branch->body);
			$this->popIndent();
			$str .= $this->indent() . "}\n";
			$cases[] = $str;
		}
		if ($n->defaultBody) {
			$str = "{\n";
			$this->pushIndent();
			$str .= $this->writeStatement($n->defaultBody);
			$this->popIndent();
			$str .= $this->indent() . "}\n";
			$cases[] = $str;
		}
		return
			$this->indent() . sprintf("\$%s = %s;\n", $varname, $this->writeExpression($n->condition)) .
			$this->indent() . implode($this->indent() . 'else', $cases);
	}

	private function writePushScope() {
		return $this->indent() .  "\$scope = \$scope->push();\n";
	}

	private function writePopScope() {
		return $this->indent() .  "\$scope = \$scope->pop();\n";
	}

	private function writeFillLocalScope($selectExpr, $localName) {
		if ($localName) {
			return sprintf("%s\$scope->__set(%s, %s);\n",
					$this->indent(),
					$this->quote($localName),
					$selectExpr);
		}
		else {
			return sprintf("%s\$scope->import(%s);\n",
					$this->indent(),
					$selectExpr);
		}
	}

	private function writeWith(WithStatement $n) {
		$str = $this->writePushScope();
		$str .= $this->writeFillLocalScope($this->writeExpression($n->selectExpr), $n->localName) .
			$this->writeStatement($n->body);
		$str .= $this->writePopScope();
		return $str;
	}

	private function writeFor(ForStatement $n) {
		$iteree = $this->nextLocalVarname();
		$localVarname = $this->nextLocalVarname();
		$str = '';

		$str .= $this->indent();
		$str .= "\$$iteree = ";
		$str .= $this->writeExpression($n->selectExpr);
		$str .= ";\n";
		$str .= $this->indent();
		$str .= "if (is_array(\$$iteree)) {\n";
		$this->pushIndent();
		$str .= $this->indent();
		$str .= "foreach (\$$iteree as \$$localVarname) {\n";
		$this->pushIndent();
		$str .= $this->writePushScope();
		$str .= $this->writeFillLocalScope("\$$localVarname", $n->localName);
		$str .= $this->writeStatement($n->body);
		$str .= $this->writePopScope();
		$this->popIndent();
		$str .= $this->indent();
		$str .= "}\n";
		$this->popIndent();
		$str .= $this->indent();
		$str .= "}\n";
		return $str;
	}

	private function writeStatement(ASTNode $n) {
		if ($n instanceof ValueOfStatement) {
			return $this->writeValueOf($n);
		}
		elseif ($n instanceof BlockStatement) {
			return $this->writeBlock($n);
		}
		elseif ($n instanceof TextStatement) {
			return $this->writeText($n);
		}
		elseif ($n instanceof SwitchStatement) {
			return $this->writeSwitch($n);
		}
		elseif ($n instanceof IfStatement) {
			return $this->writeIf($n);
		}
		elseif ($n instanceof WithStatement) {
			return $this->writeWith($n);
		}
		elseif ($n instanceof ForStatement) {
			return $this->writeFor($n);
		}
	}

	private function writeBlock(BlockStatement $n) {
		$strs = array();
		foreach ($n->children as $c) {
			$strs[] = $this->writeStatement($c);
		}
		return implode('', $strs);
	}

	private function writeDef(BlockStatement $n, $methodName = '__invoke') {
		$str = '';
		$str .= $this->indent();
		$str .= "public function $methodName(\$scope) {\n";
		$this->pushIndent();
		$str .= $this->writeStatement($n);
		$this->popIndent();
		$str .= $this->indent();
		$str .= "}\n";
		return $str;
	}

	public function write(ASTNode $n) {
		$str = $this->header;
		$str .= "\n";
		if ($this->classname) {
			$str .= $this->indent();
			$str .= "class {$this->classname} {\n";
			$this->pushIndent();
			$str .= $this->writeDef($n);
			$this->popIndent();
			$str .= $this->indent();
			$str .= "}\n";
		}
		else {
			$str .= $this->writeStatement($n);
		}

		$str .= $this->footer;

		return $str;
	}

}
