<?php

namespace Paco;

class Optimizer {
	public static function optimize(ASTNode $n) {
		if ($n instanceof ASTExpression) {
			return self::optimizeExpression($n, new Scope(null));
		}
		else {
			return self::optimizeStatement($n, new Scope(null));
		}
	}

	public static function optimizeExpression(ASTExpression $n, Scope $constScope) {
		if ($n->isConstant()) {
			$value = $n->evaluate($constScope);
			return new LiteralExpression($value);
		}
		if ($n instanceof BinaryExpression) {
			$lhs = self::optimizeExpression($n->lhs, $constScope);
			$rhs = self::optimizeExpression($n->rhs, $constScope);
			return new BinaryExpression($n->operator, $lhs, $rhs);
		}
		if ($n instanceof FunctionCallExpression) {
			$function = self::optimizeExpression($n->function, $constScope);
			$nn = new FunctionCallExpression($function);
			foreach ($n->arguments as $argExp) {
				$nn->addArgument(self::optimizeExpression($argExp, $constScope));
			}
			return $nn;
		}
		return $n;
	}

	private static function optimizeForStatement(ForStatement $n, Scope $constScope) {
		$selectExpr = self::optimizeExpression($n->selectExpr, $constScope);
		if ($selectExpr->isConstant()) {
			$body = new BlockStatement();
			foreach ($selectExpr->evaluate($constScope) as $item) {
				$constScope = $constScope->push();
				$constScope->__set($n->localName, $item);
				$bodyPart = new WithStatement(new LiteralExpression($item), $n->localName, $n->body);
				$bodyPart = self::optimizeStatement($bodyPart, $constScope);
				$body->append($bodyPart);
				$constScope = $constScope->pop();
			}
			return self::optimizeStatement($body, $constScope);
		}
		else {
			$body = self::optimizeStatement($n->body, $constScope);
			$nn = new ForStatement($selectExpr, $n->localName, $body);
		}
		return $nn;
	}

	private static function flattenBlockStatement(BlockStatement $n, Scope $constScope) {
		$nn = new BlockStatement();
		foreach ($n->children as $child) {
			$child = self::optimizeStatement($child, $constScope);
			if ($child instanceof BlockStatement) {
				foreach ($child->children as $grandchild) {
					$nn->append($grandchild);
				}
			}
			else {
				$nn->append($child);
			}
		}
		return $nn;
	}

	private static function optimizeBlockStatement(BlockStatement $n, Scope $constScope) {
		$n = self::flattenBlockStatement($n, $constScope);
		$curChild = null;
		$nn = new BlockStatement();
		foreach ($n->children as $child) {
			$prevChild = $curChild;
			$curChild = self::optimizeStatement($child, $constScope);
			if ($prevChild instanceof TextStatement && $curChild instanceof TextStatement) {
				// merge text statements
				$curChild = new TextStatement($prevChild->text . $curChild->text);
			}
			elseif ($prevChild instanceof ValueOfStatement && $curChild instanceof ValueOfStatement &&
					// merge value-of statements
					$prevChild->encodeMode === $curChild->encodeMode) {
				$curChild = new ValueOfStatement(new BinaryExpression('++', $prevChild->expr, $curChild->expr), $curChild->encodeMode);
			}
			else {
				if ($prevChild)
					$nn->append($prevChild);
			}
		}
		if ($curChild)
			$nn->append($curChild);
		return $nn;
	}

	private static function optimizeWithStatement(WithStatement $n, Scope $constScope) {
		$selectExpr = self::optimizeExpression($n->selectExpr, $constScope);
		$constScope = $constScope->push();
		if ($selectExpr->isConstant()) {
			if ($n->localName)
				$constScope->__set($n->localName, $selectExpr->evaluate($constScope));
			else
				$constScope->import($selectExpr->evaluate($constScope));
		}
		$body = self::optimizeStatement($n->body, $constScope);
		$constScope = $constScope->pop();
		return new WithStatement($selectExpr, $n->localName, $body);
	}

	private static function optimizeIfStatement(IfStatement $n, Scope $constScope) {
		$cond = self::optimizeExpression($n->condition, $constScope);
		$trueBranch = self::optimizeStatement($n->trueBranch, $constScope);
		$falseBranch = self::optimizeStatement($n->falseBranch, $constScope);
		return new IfStatement($cond, $trueBranch, $falseBranch);
	}

	public static function optimizeStatement($n, Scope $constScope) {
		if (!$n) {
			return null;
		}
		if ($n->isConstant()) {
			$value = $n->executeToString($constScope);
			return new TextStatement($value);
		}
		if ($n instanceof ValueOfStatement) {
			return new ValueOfStatement(self::optimizeExpression($n->expr, $constScope), $n->encodeMode);
		}
		if ($n instanceof IfStatement) {
		}
		if ($n instanceof WithStatement) {
			return self::optimizeWithStatement($n, $constScope);
		}
		if ($n instanceof ForStatement) {
			return self::optimizeForStatement($n, $constScope);
		}
		if ($n instanceof BlockStatement) {
			return self::optimizeBlockStatement($n, $constScope);
		}
		return $n;
	}
}
