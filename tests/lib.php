<?php

require_once dirname(__FILE__) . '/../paco.php';
require_once dirname(__FILE__) . '/../scope.php';

$_classNumber = 0;

function testFromString($template, $context, $expected, $filename = '<input>', $includeHandler = null) {
	try {
		global $_classNumber;
		// printf("------\n%s\n------\n", $template);
		$className = 'Template' . $_classNumber++;
		$ast = Paco\Parser::parse($template, $filename, $includeHandler);
	}
	catch (Paco\FatalError $ex) {
		fprintf(STDERR, "%s\nParser error: %s\n", $template, $ex->getMessage());
		return -1;
	}
	try {
		// var_dump($ast);
		$writer = new Paco\PhpWriter($className);
		$compiled = $writer->write($ast);
		// printf("------\n%s\n------\n", $compiled);
		fflush(STDOUT);
		fflush(STDERR);
	}
	catch (Paco\FatalError $ex) {
		fprintf(STDERR, "%s\nPHP writer error: %s\n", $template, $ex->getMessage());
		return -2;
	}
	try {
		$scope = new Paco\Scope($context);
		$actual = $ast->executeToString($scope);
		fflush(STDOUT);
		fflush(STDERR);
		if ($actual !== $expected) {
			fprintf(STDERR, "%s\nExpectation failed while interpreting template: '%s' !== '%s'\n", $template, $actual, $expected);
			return -3;
		}
	}
	catch (Paco\FatalError $ex) {
		fprintf(STDERR, "%s\nError interpreting template: %s\n", $template, $ex->getMessage());
		return -4;
	}
	try {
		eval($compiled);
		fflush(STDOUT);
		fflush(STDERR);
		$inst = new $className();
		$scope = new Paco\Scope($context);
		ob_start();
		$inst($scope);
		$actual = ob_get_clean();
		fflush(STDOUT);
		fflush(STDERR);
		if ($actual !== $expected) {
			fprintf(STDERR, "%s\nExpectation failed in compiled template: '%s' !== '%s'\n", $template, $actual, $expected);
			return -3;
		}
	}
	catch (Paco\FatalError $ex) {
		fprintf(STDERR, "%s\nError running compiled template: %s\n", $template, $ex->getMessage());
		return -4;
	}
	try {
		$ast2 = Paco\Optimizer::optimize($ast);
	}
	catch (Paco\FatalError $ex) {
		fprintf(STDERR, "%s\nError optimizing template: %s\n", $template, $ex->getMessage());
		return -4;
	}
	try {
		$scope = new Paco\Scope($context);
		$actual = $ast2->executeToString($scope);
		fflush(STDOUT);
		fflush(STDERR);
		if ($actual !== $expected) {
			fprintf(STDERR, "%s\nExpectation failed while interpreting optimized template: '%s' !== '%s'\n", $template, $actual, $expected);
			return -3;
		}
	}
	catch (Paco\FatalError $ex) {
		fprintf(STDERR, "%s\nError interpreting optimized template: %s\n", $template, $ex->getMessage());
		return -4;
	}
	return 0;
}
