<?php

include dirname(__FILE__) . '/lib.php';

$cases = array(
	array('foo', 'foo'),
	array(1, '1'),
	array(array(1, 2, 3), '1 2 3'),
);

foreach ($cases as $case) {
	$actual = Paco\Scope::flatten($case[0]);
	$expected = $case[1];
	if ($actual !== $expected) {
		fprintf(STDERR, "Expectation failed: '$actual' !== '$expected'\n");
		exit(-1);
	}
}
