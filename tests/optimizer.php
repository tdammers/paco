<?php

include dirname(__FILE__) . '/lib.php';

$expr = new Paco\ListLiteralExpression();
$expr->append(new Paco\LiteralExpression(1));
$expr->append(new Paco\LiteralExpression(2));
$expr->append(new Paco\LiteralExpression(3));

$exprOpt = Paco\Optimizer::optimize($expr);

$eval = $expr->evaluate(new Paco\Scope());
$evalOpt = $exprOpt->evaluate(new Paco\Scope());

fprintf(STDERR, "expr is %s\n", $expr->isConstant() ? 'constant' : 'variable');
fprintf(STDERR, "exprOpt is %s\n", $exprOpt->isConstant() ? 'constant' : 'variable');

if ($eval !== $evalOpt) {
	fprintf(STDERR, "'$eval' !== '$evalOpt'. Not good.\n");
	exit -1;
}
