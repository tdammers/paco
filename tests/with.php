<?php

require_once dirname(__FILE__) . '/lib.php';

$data = array(
	"foo" => array("bar" => "baz")
);

$testcases = array(
	array("{%with foo : f%}{f.bar}{%endwith%}", $data, 'baz'),
	array("{%with foo%}{bar}{%endwith%}", $data, 'baz'),
	array("{%with foo.bar%}{.}{%endwith%}", $data, 'baz'),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);
