<html>
<body>
<div>
Some {odd} text and {such}.
Here's a text node with a single quote in it: {"'"}
</div>
<div>
{[1, 2, 3]}
</div>
<div>
{%if number_one > number_two%}
Number One ({number_one}) is larger.
{%else%}
Number Two ({number_two}) is larger.
{%endif%}
</div>
<div>
{%switch '1'%}
	{%case number_one%}
	Number One Wins!
	{%endcase%}
	{%case number_two%}
	Number Two Wins!
	{%endcase%}
	{%default%}
	Nobody Wins.
	{%endcase%}
{%endswitch%}
</div>
<div>
Something is {something}.
Something parenthesized is {(something)}.
Something parenthesized twice is {((something))}.
</div>
{%--
This is a comment.
{%-- Comments can be nested. --%}
{% Comments can include syntax, even if it's invalid.
-- Two dashes are also OK.
--%}
<div>
{'html-escaped <> &amp;'}
<br/>
{!'raw <> &amp;'}
<br/>
{@'url-encoded <> &amp;'}
</div>
<div>
{number_one} + {number_two} = {number_one + number_two}
</div>
{%with foo : bar%}
	<ul>
	{%for bar : baz%}
		<li>{baz}</li>
	{%endfor%}
	</ul>
{%endwith%}
{%with 1 : i%}
With: {i}.
{%endwith%}
<ol>
{%for [1, 2, 3] : i%}
	<li>Item {i}</li>
{%endfor%}
</ol>
</body>
</html>
