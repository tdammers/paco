<?php

require_once dirname(__FILE__) . '/lib.php';

$testcases = array(
	array('{%-- --%}', array(), ''),
	array('{%----%}', array(), ''),
	array('{%-- { --%}', array(), ''),
	array('{%-- {% --%}', array(), ''),
	array('{%-- %} --%}', array(), ''),
	array('{%-- {%-- --%} --%}', array(), ''),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);


