<?php

require_once dirname(__FILE__) . '/lib.php';

class TestClass {
	public function sayHello($name) {
		return sprintf("Hello, %s!", $name);
	}
}

$data = array(
	'sys' => array(
		'str_pad' => new Paco\FunctionPtr('str_pad'),
		'str_repeat' => new Paco\FunctionPtr('str_repeat'),
	),
	'test' => new TestClass(),
);

$testcases = array(
	array("{sys.str_pad('foo', 5)}", $data, 'foo  '),
	array("{sys.str_repeat('a', 3)}", $data, 'aaa'),
	array("{sys.str_pad('foo' 5)}", $data, 'foo  '),
	array("{\$sys.str_pad 'foo' 5}", $data, 'foo  '),
	array("{\$sys.str_repeat 1 2 + 1}", $data, '111'),
	array("{(\$sys.str_repeat 1 2) + 1}", $data, '12'),
	array("{test.sayHello('Rasmus')}", $data, 'Hello, Rasmus!'),
	array("{sys.notafunction('Hello')}", $data, ''),
	array("{sys.strcmp('Hello', 'World')}", $data, ''),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);


