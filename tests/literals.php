<?php

require_once dirname(__FILE__) . '/lib.php';

$testcases = array(
	array('{"foobar"}', 'foobar'),
	array("{'foobar'}", 'foobar'),
	array('{1}', '1'),
	array('{-10}', '-10'),
	array('{0}', '0'),
	array('{1.5}', '1.5'),
	array('{[1, 2, 3]}', '1 2 3'),
	array('{%for [1, 2, 3] : i%}({i}){%endfor%}', '(1)(2)(3)'),
	array('{{"foo":1, "bar" : 2}}', '1 2'),
	array('{%with {"foo":1, "bar":2}%}{foo}, {bar}{%endwith%}', '1, 2'),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], array(), $t[1])) {
		--$failed;
	}
}

exit($failed);
