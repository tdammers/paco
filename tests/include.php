<?php

include dirname(__FILE__) . '/lib.php';

$outerTemplate = <<<TPL
<outer>
{%include inner%}
{%include inner with foo %}
{%include inner with bar : value %}
</outer>
TPL;

$innerTemplate = "{value}\n";

class MyIncludeHandler {
	public function loadInclude($includeName, &$filename = null) {
		if ($includeName === 'inner') {
			global $innerTemplate;
			return $innerTemplate;
		}
		else {
			return "Nope!";
		}
	}
}

$ast = Paco\Parser::parse($outerTemplate, '<input>', new MyIncludeHandler());
$scope = new Paco\Scope(array(
	'value' => '23',
	'foo' => array('value' => '42'),
	'bar' => '67'));

$actual = $ast->executeToString($scope);
$expected = <<<TPL
<outer>
23
42
67
</outer>
TPL;

if ($actual !== $expected) {
	fprintf(STDERR, "Expectation failed\n");
	fprintf(STDERR, "$actual\n");
	exit(-1);
}
