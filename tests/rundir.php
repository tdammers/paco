<?php

$dir = $argv[1];
$dir = preg_replace('/\\/$/', '', $dir);
include dirname(__FILE__) . '/lib.php';

$total = $failed = $succeeded = 0;

class MyIncludeHandler {
	public function __construct($dir) {
		$this->dir = $dir;
	}

	public function loadInclude($templateName) {
		$filename = $this->dir . "/include/$templateName.tpl";
		if (!file_exists($filename)) {
			fprintf(STDERR, "File not found: $filename\n");
			exit -1;
		}
		return file_get_contents($filename);
	}
}

$includeHandler = new MyIncludeHandler($dir);
fprintf(STDERR, "\n");

foreach (glob($dir . '/*.tpl') as $srcfn) {
	fprintf(STDERR, "%s\n", basename($srcfn));
	$dstfn = preg_replace('/\.tpl$/', '.html', $srcfn);
	$src = file_get_contents($srcfn);
	$expected = file_get_contents($dstfn);
	if (testFromString($src, array(), $expected, $srcfn, $includeHandler)) {
		++$failed;
	}
	else {
		++$succeeded;
	}
	++$total;
}

exit ($failed);
