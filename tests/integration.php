<?php

require_once dirname(__FILE__) . '/../paco.php';

$template = file_get_contents(dirname(__FILE__) . '/integration.tpl');

$context = array(
	"foo" => array("one", "two", "three"),
	"number_one" => 1,
	"number_two" => 3,
	"odd" => "<strong>odd</strong>",
	"such" => ">>>",
	"something" => true,
	"defy" => "<strong>defy</strong>",
	);

$timing = array();
$baseline = 0.0;

function beginPhase($name) {
	global $timing;
	fprintf(STDERR, "$name...");
	$timing[$name][0] = microtime(true);
}

function endPhase($name) {
	global $timing;
	$timing[$name][1] = microtime(true);
	fprintf(STDERR, "\rDone %s.%s\n", $name, str_repeat(' ', 60));
}

function printDiff($left, $right) {
	$leftFilename = tempnam('/tmp', false);
	$rightFilename = tempnam('/tmp', false);
	file_put_contents($leftFilename, $left);
	file_put_contents($rightFilename, $right);
	$diff = `diff -y $leftFilename $rightFilename`;
	print($diff);
	unlink($leftFilename);
	unlink($rightFilename);
}

beginPhase('Timing baseline');
endPhase('Timing baseline');

beginPhase('Parsing');
$ast = Paco\Parser::parse($template);
endPhase('Parsing');

beginPhase('Compiling');
$writer = new Paco\PhpWriter();
$compiled = $writer->write($ast);
endPhase('Compiling');

file_put_contents(dirname(__FILE__) . '/../compiled-templates/integration.php', $compiled);

beginPhase('Optimizing');
$astOpt = Paco\Optimizer::optimize($ast);
endPhase('Optimizing');

beginPhase('Compiling optimized');
$writer = new Paco\PhpWriter();
$compiledOpt = $writer->write($astOpt);
endPhase('Compiling optimized');

file_put_contents(dirname(__FILE__) . '/../compiled-templates/integration-opt.php', $compiledOpt);

beginPhase('Interpreting');
$scope = new Paco\Scope($context);
$interpreted = $ast->executeToString($scope);
endPhase('Interpreting');

beginPhase('Interpreting optimized');
$scope = new Paco\Scope($context);
$interpretedOpt = $astOpt->executeToString($scope);
endPhase('Interpreting optimized');

beginPhase('Running');
$scope = new Paco\Scope($context);
ob_start();
eval($compiled);
$executed = ob_get_clean();
endPhase('Running');

beginPhase('Running optimized');
$scope = new Paco\Scope($context);
ob_start();
eval($compiledOpt);
$executedOpt = ob_get_clean();
endPhase('Running optimized');

if ($interpreted !== $interpretedOpt) {
	fprintf(STDERR, "Output mismatch: interpreted != interpreted-optimized\n");
	printDiff($interpreted, $interpretedOpt);
	exit(-1);
}
if ($interpreted !== $executed) {
	fprintf(STDERR, "Output mismatch: interpreted != compiled\n");
	printDiff($interpreted, $executed);
	exit(-1);
}
if ($interpreted !== $executedOpt) {
	fprintf(STDERR, "Output mismatch: interpreted != compiled-optimized\n");
	printDiff($interpreted, $executedOpt);
	exit(-1);
}

$baseline = $timing['Timing baseline'][1] - $timing['Timing baseline'][0];

printf("---- Raw timings ----\n");
foreach ($timing as $n => $t) {
	$real_timing[$n] = $t[1] - $t[0] - $baseline;
	printf("%s: %1.8f µs\n", $n, ($t[1] - $t[0] - $baseline) * 1000000);
}

$scenario_timing['interpret'] =
	 $real_timing['Parsing'] +
	 $real_timing['Interpreting'];
$scenario_timing['interpret-optimized'] =
	 $real_timing['Parsing'] +
	 $real_timing['Optimizing'] +
	 $real_timing['Interpreting optimized'];
$scenario_timing['compile-and-run'] =
	 $real_timing['Parsing'] +
	 $real_timing['Compiling'] +
	 $real_timing['Running'];
$scenario_timing['compile-and-run-optimized'] =
	 $real_timing['Parsing'] +
	 $real_timing['Optimizing'] +
	 $real_timing['Compiling optimized'] +
	 $real_timing['Running optimized'];
$scenario_timing['run-precompiled'] =
	 $real_timing['Running'];
$scenario_timing['run-precompiled-optimized'] =
	 $real_timing['Running optimized'];
	
$max_scenario = $scenario_timing['interpret'];

printf("---- Usage scenarios ----\n");
foreach ($scenario_timing as $n => $t) {
	printf("%40s: %5.0f µs (time: %5.1f%% / speed: %5.0f%%)\n", $n, $t * 1000000, $t * 100 / $max_scenario, $max_scenario * 100 / $t);
}

