<?php

require_once dirname(__FILE__) . '/lib.php';

$testcases = array(
	array('{1}', array(), '1'),
	array('{ 1}', array(), '1'),
	array('{ 1}', array(), '1'),
	array('{ 1 }', array(), '1'),
	array('{1+1}', array(), '2'),
	array('{ 1 + 1 }', array(), '2'),
	array('{ 1+1 }', array(), '2'),
	array('{1 + 1}', array(), '2'),
	array('{1+ 1}', array(), '2'),
	array('{1 +1}', array(), '2'),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);

