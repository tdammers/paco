<?php

require_once dirname(__FILE__) . '/lib.php';

$testcases = array(
	array('{a}', array('a' => 'foobar'), 'foobar'),
	array('{a}', array('a' => '<>&'), '&lt;&gt;&amp;'),
	array('{!a}', array('a' => '<>&'), '<>&'),
	array('{@a}', array('a' => 'a b'), 'a%20b'),
	array('{.}', 'foobar', 'foobar'),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);
