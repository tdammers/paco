<?php

include dirname(__FILE__) . '/lib.php';

class TestParser extends Paco\Parser {
	public static function runTest() {
		$s = new Paco\ParserState('1+1', '<input>');
		$node = self::parseBinaryExpressionL($s, 'parseSimpleExpression', '', array('+'));
		return 0;
	}
}

return TestParser::runTest();
