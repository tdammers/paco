<?php

require_once dirname(__FILE__) . '/lib.php';

$data = array(
	"foo" => array("bar", "baz")
);

$testcases = array(
	array("{%for foo : f%}{f}{%endfor%}", $data, 'barbaz'),
	array("{%for foo%}{.}{%endfor%}", $data, 'barbaz'),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);
