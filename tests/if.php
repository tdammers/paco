<?php

require_once dirname(__FILE__) . '/lib.php';

$testcases = array(
	array("{%if a%}foobar{%endif%}", array(), ''),
	array("{%if a%}foobar{%endif%}", array('a' => '1'), 'foobar'),
	array("{%if a%}foobar{%endif%}", array('a' => false), ''),
	array("{%if a%}foobar{%else%}baz{%endif%}", array('a' => false), 'baz'),
	array("{%if a%}foobar{%else%}baz{%endif%}", array('a' => true), 'foobar'),
	array('{%if foo%}1{%endif%}', array('foo' => 1, 'bar' => 1), '1'),
	array('{%if foo + bar%}1{%endif%}', array('foo' => 1, 'bar' => 1), '1'),
	array('{%if foo > bar%}1{%endif%}', array('foo' => 1, 'bar' => 1), ''),
	array('{%if foo = bar%}1{%else%}0{%endif%}', array('foo' => 1, 'bar' => 1), '1'),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);

