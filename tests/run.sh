#!/usr/bin/env bash
cd `dirname $0`

FAILED=0
SUCCEEDED=0

# Files directly under /tests: these are run directly
FILES=*.php
for f in $FILES
do
	echo -n "$f - "
	if [[ $f = 'rundir.php' || $f = 'lib.php' ]]
		then
			echo "(skip)"
			continue
		fi
	php $f # > /dev/null 2> /dev/null
	if (( $? ))
		then
			echo "FAIL"
			FAILED=$(( $FAILED + 1 ))
		else
			echo "OK"
			SUCCEEDED=$(( $SUCCEEDED + 1 ))
		fi
done

# Rundirs under /tests: these are run directly
FILES=*.rundir
for f in $FILES
do
	echo -n "$f - "
	php rundir.php $f
	if (( $? ))
		then
			echo "FAIL"
			FAILED=$(( $FAILED + 1 ))
		else
			echo "OK"
			SUCCEEDED=$(( $SUCCEEDED + 1 ))
		fi
done
echo "FAIL: $FAILED"
echo "SUCCESS: $SUCCEEDED"
exit $FAILED
