<?php

require_once dirname(__FILE__) . '/lib.php';

$data = array(
	"foo" => array("bar" => "A"),
	"baz" => array("quux" => "B"),
);

$testcases = array(
	array("{%with foo, baz%}{bar}{quux}{%endwith%}", $data, 'AB'),
	array("{%with 1 : quux, 2 : bar %}{bar}{quux}{%endwith%}", $data, '21'),
	array("{%with {'quux':1, 'bar':2} %}{bar}{quux}{%endwith%}", $data, '21'),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);
