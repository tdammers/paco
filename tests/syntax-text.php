<?php

require_once dirname(__FILE__) . '/lib.php';

$testcases = array(
	array('Plain text', 'Plain text'),
	array('<html>', '<html>'),
	array('&amp;', '&amp;'),
	array("\n", "\n"),
	array('{"foo"}', 'foo'),
	array('{"<html>"}', '&lt;html&gt;'),
	array('{0}', '0'),
	array('{1}', '1'),
	array('{-1}', '-1'),
	array('{@1}', '1'),
	array('{"\'"}', "'"),
	array('{"<html>"}', '&lt;html&gt;'),
	array('{!"<html>"}', '<html>'),
	array("<{'foobar'}>", '<foobar>'),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], array(), $t[1])) {
		--$failed;
	}
}

exit($failed);
