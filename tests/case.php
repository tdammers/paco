
<?php

require_once dirname(__FILE__) . '/lib.php';

$testcases = array(
	array("{%switch 'a'%}{%case 'b'%}B!{%endcase%}{%case 'a'%}A!{%endcase%}{%endswitch%}", array(), "A!"),
	array("{%switch foobar%}{%case 'b'%}B!{%endcase%}{%case 'a'%}A!{%endcase%}{%endswitch%}", array('foobar' => 'a'), "A!"),
	array("{%switch '1'%}{%case 'x'%}X{%endcase%}{%default%}Y{%endcase%}{%endswitch%}", array(), "Y"),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);
