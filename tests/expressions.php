<?php

require_once dirname(__FILE__) . '/lib.php';

$testcases = array(
	array('{(1)}', array(), '1'),
	array('{((1))}', array(), '1'),
	array('{1 + 2}', array(), '3'),
	array('{1 - 2}', array(), '-1'),
	array('{1 * 2}', array(), '2'),
	array('{1 / 2}', array(), '0.5'),
	array('{3 + 2}', array(), '5'),
	array('{3 - 2}', array(), '1'),
	array('{3 * 2}', array(), '6'),
	array('{3 / 2}', array(), '1.5'),
	array('{3 + 4 + 5}', array(), '12'),
	array('{3 - 4 - 5}', array(), '-6'),
	array('{(3 + 4) * 5}', array(), '35'),
	array('{3 + 4 * 5}', array(), '23'),
	array('{3 * 4 + 5}', array(), '17'),
	array('{3 * (4 + 5)}', array(), '27'),

	array('{1 && 1}', array(), '1'),
	array('{1 && 0}', array(), ''),
	array('{0 && 0}', array(), ''),
	array('{1 || 1}', array(), '1'),
	array('{1 || 0}', array(), '1'),
	array('{0 || 0}', array(), ''),

	array('{1 and 1}', array(), '1'),
	array('{1 and 0}', array(), ''),
	array('{0 and 0}', array(), ''),
	array('{1 or 1}', array(), '1'),
	array('{1 or 0}', array(), '1'),
	array('{0 or 0}', array(), ''),

	array('{"foo" ++ "bar"}', array(), 'foobar'),

	array('{foo["bar"]}', array('foo' => array('bar' => 'baz')), 'baz'),
	array('{foo["bar"]["baz"]}', array('foo' => array('bar' => array('baz' => 'quux'))), 'quux'),
	array('{foo.bar.baz}', array('foo' => array('bar' => array('baz' => 'quux'))), 'quux'),
	array('{foo["bar"].baz}', array('foo' => array('bar' => array('baz' => 'quux'))), 'quux'),

	array('{1 + 1 && 1 - 1}', array(), ''),
	array('{1 + 1 || 1 - 1}', array(), '1'),
	array("{'/[a-z]/' ~= 'foobar'}", array(), '1'),
	array('{-a}', array('a' => 2), '-2'),
	array('{not 1}', array(), ''),
	array('{not 0}', array(), '1'),

	array('{foo in bar}', array('foo' => 1, 'bar' => array(1, 2, 3)), '1'),
	array('{bar contains foo}', array('foo' => 1, 'bar' => array(1, 2, 3)), '1'),
);

$failed = 0;
foreach ($testcases as $t) {
	if (testFromString($t[0], $t[1], $t[2])) {
		--$failed;
	}
}

exit($failed);

