<?php

namespace Paco;

define('ENCODE_HTML', 1);
define('ENCODE_URL', 2);
define('ENCODE_RAW', 3);

interface Writer {
	function write(ASTNode $n);
}

interface ASTNode {
	function isConstant();
}

interface ASTExpression extends ASTNode {
	function evaluate(Scope $scope);
}

interface ASTStatement extends ASTNode {
	function executeToString(Scope $scope);
}

class ListLiteralExpression implements ASTExpression {
	private $items;

	public function __get($key) {
		switch ($key) {
			case 'items': return $this->items;
			default: return parent::__get($key);
		}
	}

	public function __construct() {
		$this->items = array();
	}

	public function append(ASTExpression $item) {
		$this->items[] = $item;
	}

	public function isConstant() {
		foreach ($this->items as $item) {
			if (!$item->isConstant()) {
				return false;
			}
		}
		return true;
	}

	public function evaluate(Scope $scope) {
		$values = array();
		foreach ($this->items as $item) {
			$values[] = $item->evaluate($scope);
		}
		return $values;
	}
}

class MapLiteralExpression implements ASTExpression {
	private $items;

	public function __get($key) {
		switch ($key) {
			case 'items': return $this->items;
			default: return parent::__get($key);
		}
	}

	public function __construct() {
		$this->items = array();
	}

	public function append(ASTExpression $keyExpr, ASTExpression $valExpr) {
		$this->items[] = array($keyExpr, $valExpr);
	}

	public function isConstant() {
		foreach ($this->items as $item) {
			if (!$item[0]->isConstant() || !$item[1]->isConstant()) {
				return false;
			}
		}
		return true;
	}

	public function evaluate(Scope $scope) {
		$values = array();
		foreach ($this->items as $item) {
			$key = $item[0]->evaluate($scope);
			$val = $item[1]->evaluate($scope);
			$values[$key] = $val;
		}
		return $values;
	}
}

class LiteralExpression implements ASTExpression {
	private $value;

	public function __get($key) {
		switch ($key) {
			case 'value': return $this->value;
			default: return parent::__get($key);
		}
	}

	public function __construct($value) {
		$this->value = $value;
	}

	public function isConstant() { return true; }

	public function evaluate(Scope $scope) {
		return $this->value;
	}
}

class VariableExpression implements ASTExpression {
	private $varname;

	public function __get($key) {
		switch ($key) {
			case 'varname': return $this->varname;
			default: return parent::__get($key);
		}
	}

	public function __construct($varname) {
		$this->varname = $varname;
	}

	public function isConstant() { return false; }

	public function evaluate(Scope $scope) {
		if ($this->varname === '.')
			return $scope->getSelf();
		else
			return $scope->__get($this->varname);
	}
}

class FunctionCallExpression implements ASTExpression {
	private $function;
	private $arguments;
	private $const;

	public function __get($key) {
		switch ($key) {
			case 'function': return $this->function;
			case 'arguments': return $this->arguments;
			default: return parent::__get($key);
		}
	}

	public function __construct(ASTExpression $function) {
		$this->function = $function;
		$this->arguments = array();
		$this->const = ($function->isConstant());
	}

	public function addArgument(ASTExpression $argument) {
		$this->arguments[] = $argument;
		if (!$argument->isConstant()) {
			$this->const = false;
		}
	}

	public function isConstant() {
		return $this->const;
	}

	public function evaluate(Scope $scope) {
		$func = $this->function->evaluate($scope);
		if (!$func instanceof FunctionPtr) {
			return '';
		}
		$args = array();
		foreach ($this->arguments as $argExp) {
			$args[] = $argExp->evaluate($scope);
		}
		return call_user_func_array($func, $args);
	}
}

class UnaryExpression implements ASTExpression {
	private $rhs;
	private $operator;

	public function __get($key) {
		switch ($key) {
			case 'rhs': return $this->rhs;
			case 'operator': return $this->operator;
			default: return parent::__get($key);
		}
	}

	public function __construct($operator, ASTExpression $rhs) {
		$this->operator = $operator;
		$this->rhs = $rhs;
	}

	public function isConstant() {
		switch ($this->operator) {
			default:
				return $this->rhs->isConstant();
		}
	}

	public function evaluate(Scope $scope) {
		switch ($this->operator) {
			case '!': return !($this->rhs->evaluate($scope));
			case 'not': return !($this->rhs->evaluate($scope));
			case '-': return -($this->rhs->evaluate($scope));
			case '~': return ~($this->rhs->evaluate($scope));
		}
	}
	
}

class BinaryExpression implements ASTExpression {
	private $lhs;
	private $rhs;
	private $operator;

	public function __get($key) {
		switch ($key) {
			case 'lhs': return $this->lhs;
			case 'rhs': return $this->rhs;
			case 'operator': return $this->operator;
			default: return parent::__get($key);
		}
	}

	public function __construct($operator, ASTExpression $lhs, ASTExpression $rhs) {
		$this->operator = $operator;
		$this->lhs = $lhs;
		$this->rhs = $rhs;
	}

	public function isConstant() {
		switch ($this->operator) {
			case '||':
			case 'or':
				return ($this->lhs->isConstant() && $this->lhs->evaluate(new Scope(null))) ||
					   ($this->rhs->isConstant() && $this->rhs->evaluate(new Scope(null)));
			case '&&':
			case 'and':
				return ($this->lhs->isConstant() && !$this->lhs->evaluate(new Scope(null))) ||
					   ($this->rhs->isConstant() && !$this->rhs->evaluate(new Scope(null)));
			default:
				return $this->lhs->isConstant() && $this->rhs->isConstant();
		}
	}

	public function evaluate(Scope $scope) {
		switch ($this->operator) {
			case '!=': return $this->lhs->evaluate($scope) != $this->rhs->evaluate($scope);
			case '!==': return $this->lhs->evaluate($scope) !== $this->rhs->evaluate($scope);
			case '&&': return $this->lhs->evaluate($scope) && $this->rhs->evaluate($scope);
			case '&': return $this->lhs->evaluate($scope) & $this->rhs->evaluate($scope);
			case '*': return $this->lhs->evaluate($scope) * $this->rhs->evaluate($scope);
			case '+': return $this->lhs->evaluate($scope) + $this->rhs->evaluate($scope);
			case '-': return $this->lhs->evaluate($scope) - $this->rhs->evaluate($scope);
			case '/': return $this->lhs->evaluate($scope) / $this->rhs->evaluate($scope);
			case '<': return $this->lhs->evaluate($scope) < $this->rhs->evaluate($scope);
			case '<=': return $this->lhs->evaluate($scope) <= $this->rhs->evaluate($scope);
			case '<>': return $this->lhs->evaluate($scope) <> $this->rhs->evaluate($scope);
			case '>': return $this->lhs->evaluate($scope) > $this->rhs->evaluate($scope);
			case '>=': return $this->lhs->evaluate($scope) >= $this->rhs->evaluate($scope);
			case 'and': return $this->lhs->evaluate($scope) and $this->rhs->evaluate($scope);
			case 'or': return $this->lhs->evaluate($scope) or $this->rhs->evaluate($scope);
			case 'in': return in_array($this->lhs->evaluate($scope), $this->rhs->evaluate($scope));
			case 'contains': return in_array($this->rhs->evaluate($scope), $this->lhs->evaluate($scope));
			case '|': return $this->lhs->evaluate($scope) | $this->rhs->evaluate($scope);
			case '||': return $this->lhs->evaluate($scope) || $this->rhs->evaluate($scope);
			case '=': return $this->lhs->evaluate($scope) == $this->rhs->evaluate($scope);
			case '==': return $this->lhs->evaluate($scope) === $this->rhs->evaluate($scope);
			case '=~': return preg_match($this->rhs->evaluate($scope), $this->lhs->evaluate($scope));
			case '~=': return preg_match($this->lhs->evaluate($scope), $this->rhs->evaluate($scope));
			case '.': return Scope::getFrom($this->lhs->evaluate($scope), $this->rhs->evaluate($scope));
			case '++': return $this->lhs->evaluate($scope) . $this->rhs->evaluate($scope);
		}
	}
}

class TextStatement implements ASTStatement {
	private $text;

	public function __get($key) {
		switch ($key) {
			case 'text': return $this->text;
			default: return parent::__get($key);
		}
	}

	public function __construct($text) {
		$this->text = $text;
	}

	public function isConstant() { return true; }
	public function executeToString(Scope $scope) {
		return $this->text;
	}
}

class ValueOfStatement implements ASTStatement {
	private $expr;
	private $encodeMode;

	public function __get($key) {
		switch ($key) {
			case 'expr': return $this->expr;
			case 'encodeMode': return $this->encodeMode;
			default: return parent::__get($key);
		}
	}

	public function __construct(ASTExpression $expr, $encodeMode = ENCODE_HTML) {
		$this->expr = $expr;
		$this->encodeMode = $encodeMode;
	}

	public function isConstant() { return $this->expr->isConstant(); }
	public function executeToString(Scope $scope) {
		$value = Scope::flatten($this->expr->evaluate($scope));
		switch ($this->encodeMode) {
			case ENCODE_RAW:
				return $value;
			case ENCODE_URL:
				return rawurlencode($value);
			case ENCODE_HTML:
			default:
				return htmlspecialchars($value);
		}
	}
}

class CaseBranchStatement implements ASTStatement {
	private $condition;
	private $body;

	public function __construct(ASTExpression $condition, ASTStatement $body) {
		$this->condition = $condition;
		$this->body = $body;
	}

	public function __get($key) {
		switch ($key) {
			case 'condition': return $this->condition;
			case 'body': return $this->body;
			default: return parent::__get($key);
		}
	}

	public function isConstant() {
		return $this->condition->isConstant() && $this->body->isConstant();
	}

	public function executeToString(Scope $scope) {
		return $this->body->executeToString($scope);
	}
}

class SwitchStatement implements ASTStatement {
	private $condition;
	private $cases;
	private $default;

	public function __construct($condition) {
		$this->condition = $condition;
		$this->cases = array();
		$this->defaultBody = null;
	}

	public function __get($key) {
		switch ($key) {
			case 'condition': return $this->condition;
			case 'cases': return $this->cases;
			case 'defaultBody': return $this->defaultBody;
			default: return parent::__get($key);
		}
	}

	public function addBranch(CaseBranchStatement $case) {
		$this->cases[] = $case;
	}

	public function setDefault(ASTStatement $defaultBody = null) {
		$this->defaultBody = $defaultBody;
	}

	public function hasBranches() {
		return $this->defaultBody || count($this->branches);
	}

	public function isConstant() {
		if (!$this->condition->isConstant()) {
			return false;
		}
		foreach ($this->cases as $case) {
			if (!$case->isConstant()) {
				return false;
			}
		}
		if ($this->defaultBody) {
			return $this->defaultBody->isConstant();
		}
		else {
			return true;
		}
	}

	public function executeToString(Scope $scope) {
		$conditionVal = $this->condition->evaluate($scope);
		foreach ($this->cases as $case) {
			$caseVal = $case->condition->evaluate($scope);
			$isMatch = ($conditionVal == $caseVal);
			if ($isMatch) {
				return $case->body->executeToString($scope);
			}
		}
		if ($this->defaultBody) {
			return $this->defaultBody->executeToString($scope);
		}
		return '';
	}
}

class IfStatement implements ASTStatement {
	private $condition;
	private $trueBranch;
	private $falseBranch;

	public function __get($key) {
		switch ($key) {
			case 'condition': return $this->condition;
			case 'trueBranch': return $this->trueBranch;
			case 'falseBranch': return $this->falseBranch;
			default: return parent::__get($key);
		}
	}

	public function __construct(ASTExpression $condition, ASTStatement $trueBranch, ASTStatement $falseBranch = null) {
		$this->condition = $condition;
		$this->trueBranch = $trueBranch;
		$this->falseBranch = $falseBranch;
	}

	public function isConstant() {
		if (!$this->condition->isConstant()) {
			return false;
		}
		if ($this->condition->evaluate(new Scope(null))) {
			return $this->trueBranch->isConstant();
		}
		else {
			return $this->falseBranch->isConstant();
		}
	}

	public function executeToString(Scope $scope) {
		$c = $this->condition->evaluate($scope);
		if ($c) {
			return $this->trueBranch->executeToString($scope);
		}
		elseif ($this->falseBranch) {
			return $this->falseBranch->executeToString($scope);
		}
		else {
			return '';
		}
	}
}

class WithStatement implements ASTStatement {
	private $selectExpr;
	private $localName;
	private $body;

	public function __get($key) {
		switch ($key) {
			case 'selectExpr': return $this->selectExpr;
			case 'localName': return $this->localName;
			case 'body': return $this->body;
			default: return parent::__get($key);
		}
	}

	public function __construct(ASTExpression $selectExpr, $localName, ASTStatement $body) {
		$this->selectExpr = $selectExpr;
		$this->localName = $localName;
		$this->body = $body;
	}

	public function isConstant() {
		return false;
	}

	public function executeToString(Scope $scope) {
		$innerScope = $scope->push();
		if ($this->localName)
			$innerScope->__set($this->localName, $this->selectExpr->evaluate($scope));
		else
			$innerScope->import($this->selectExpr->evaluate($scope));
		return $this->body->executeToString($innerScope);
	}
}

class ForStatement implements ASTStatement {
	private $selectExpr;
	private $localName;
	private $body;

	public function __get($key) {
		switch ($key) {
			case 'selectExpr': return $this->selectExpr;
			case 'localName': return $this->localName;
			case 'body': return $this->body;
			default: return parent::__get($key);
		}
	}

	public function __construct(ASTExpression $selectExpr, $localName, ASTStatement $body) {
		$this->selectExpr = $selectExpr;
		$this->localName = $localName;
		$this->body = $body;
	}

	public function isConstant() {
		return false;
	}

	public function executeToString(Scope $scope) {
		$iteree = $this->selectExpr->evaluate($scope);
		$result = '';
		if (is_array($iteree)) {
			foreach ($iteree as $item) {
				$innerScope = $scope->push();
				if ($this->localName)
					$innerScope->__set($this->localName, $item);
				else
					$innerScope->import($item);
				$result .= $this->body->executeToString($innerScope);
			}
		}
		return $result;
	}
}

class BlockStatement implements ASTStatement {
	private $children;

	public function __get($key) {
		switch ($key) {
			case 'children': return $this->children;
			default: return parent::__get($key);
		}
	}

	public function __construct() {
		$this->children = array();
	}

	public function append(ASTStatement $child) {
		$this->children[] = $child;
	}

	public function isConstant() {
		foreach ($this->children as $child) {
			if (!$child->isConstant()) {
				return false;
			}
		}
		return true;
	}

	public function executeToString(Scope $scope) {
		$result = '';
		foreach ($this->children as $child) {
			$result .= $child->executeToString($scope);
		}
		return $result;
	}
}
